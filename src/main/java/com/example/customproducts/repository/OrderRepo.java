package com.example.customproducts.repository;

import com.example.customproducts.entity.Cart;
import com.example.customproducts.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepo extends JpaRepository<Order, Integer> {
}
