package com.example.customproducts.repository;

import com.example.customproducts.entity.Cart;
import com.example.customproducts.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepo extends JpaRepository<Product, Integer> {
    Product findById(int id);
}
