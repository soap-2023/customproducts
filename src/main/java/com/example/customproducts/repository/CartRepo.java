package com.example.customproducts.repository;
import com.example.customproducts.entity.Cart;
import com.example.customproducts.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartRepo extends JpaRepository<Cart, Integer>{
    List<Cart> findAllByUser(User user);
}
