package com.example.customproducts.repository;

import com.example.customproducts.entity.Cart;
import com.example.customproducts.entity.Product;
import com.example.customproducts.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Integer> {
    User findById(int id);
}
