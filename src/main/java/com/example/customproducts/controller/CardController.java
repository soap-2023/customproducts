package com.example.customproducts.controller;

import com.example.customproducts.entity.Cart;
import com.example.customproducts.implement.ICartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;

@RestController
public class CardController {
    @Autowired
    private ICartService cartService;

    @PostMapping("/add-cart")
    public ResponseEntity addCart(@RequestBody Cart cart) {
        try {
            if(cartService.add(cart)){
                return ResponseEntity.ok("Добавлено в корзину");
            }else{
                return ResponseEntity.ok("Ошибка добавления в корзину");
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error");
        }
    }
    @GetMapping("/getCartItemsByUserId/{id}")
    public ResponseEntity getCartItemsByUserId(@PathVariable("id") Integer userId) {
        try {
            return ResponseEntity.ok(cartService.getCartItemsByUserId(userId));
        } catch (NotFoundException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Error");
        }
    }

}
