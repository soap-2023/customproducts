package com.example.customproducts.controller;
import com.example.customproducts.implement.IPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/payment")
public class PaymentController {
    @Autowired
    IPaymentService paymentService;

    @PostMapping("/create-payment-intent")
    public ResponseEntity createPaymentIntent(@RequestParam("userId") int userId)  {
        try {
            String responseFromService = paymentService.createPaymentIntent(userId);
            return ResponseEntity.ok(responseFromService);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }

    @PostMapping("/confirm-payment")
    public ResponseEntity confirmPayment(@RequestParam("paymentIntentId") String paymentIntentId) {
        try {
            String responseFromService = paymentService.confirmPayment(paymentIntentId);
            return ResponseEntity.ok(responseFromService);
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        }
    }
}