package com.example.customproducts.implement;

import com.example.customproducts.entity.Cart;

import java.util.List;

public interface ICartService {

    boolean add(Cart cart);
    List<Cart> getCartItemsByUserId(int userId);
}
