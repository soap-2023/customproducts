package com.example.customproducts.implement;

import com.example.customproducts.entity.Cart;
import com.example.customproducts.entity.Order;

public interface IOrderService {
    boolean add(Order order);
}
