package com.example.customproducts.implement;

import com.stripe.exception.StripeException;

public interface IPaymentService {
    String createPaymentIntent(int userId) throws StripeException;
    String confirmPayment(String payId) throws StripeException;
}
