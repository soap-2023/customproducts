package com.example.customproducts.service;

import com.example.customproducts.entity.Cart;
import com.example.customproducts.implement.ICartService;
import com.example.customproducts.implement.IPaymentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stripe.Stripe;
import com.stripe.model.PaymentIntent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PaymentService implements IPaymentService {

    @Value("${stripe.api.secretKey}")
    private String secretKey;

    @Autowired
    private ICartService cartService;

    @Override
    public String createPaymentIntent(int userId) {
        try {
            Stripe.apiKey = secretKey;
            ObjectMapper objectMapper = new ObjectMapper();
            List<Cart> cardList = cartService.getCartItemsByUserId(userId);
            log.info("Список товаров на оплату {}",objectMapper.writeValueAsString(cardList));

            int totalSum = (int)cardList.stream()
                    .mapToDouble(obj -> obj.getCount() * obj.getProduct().getPrice())
                    .sum();

            String resultString = cardList.stream()
                    .map(obj -> obj.getProduct().getName() + " шт - " + (int)obj.getCount())
                    .collect(Collectors.joining(", "));


            Map<String, Object> params = new HashMap<>();
            params.put("currency", "usd");
            params.put("payment_method_types", Arrays.asList("card"));
            params.put("description", resultString);
            params.put("amount", totalSum);

            PaymentIntent paymentIntent = PaymentIntent.create(params);

            Map<String, String> response = new HashMap<>();
            response.put("paymentIntentId", paymentIntent.getId());
            return response.toString();
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String confirmPayment(String payId)  {
        try {
            Stripe.apiKey = secretKey;

            PaymentIntent paymentIntent = PaymentIntent.retrieve(payId);

            Map<String, Object> confirmParams = new HashMap<>();
            confirmParams.put("payment_method", "pm_card_visa");
            paymentIntent.confirm(confirmParams);
            Map<String, String> response = new HashMap<>();
            response.put("status", paymentIntent.getStatus());
            if (response.toString().contains("requires_payment_method")) {
                return "Успешная оплата";
            }
            return response.toString();
        } catch (Exception ex) {
            return ex.getMessage();
        }

    }
}
