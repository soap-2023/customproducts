package com.example.customproducts.service;

import com.example.customproducts.entity.Cart;
import com.example.customproducts.entity.Product;
import com.example.customproducts.entity.User;
import com.example.customproducts.implement.ICartService;
import com.example.customproducts.repository.CartRepo;
import com.example.customproducts.repository.ProductRepo;
import com.example.customproducts.repository.UserRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CartService implements ICartService {
    @Autowired
    private CartRepo cartRepo;
    @Autowired
    private ProductRepo productRepo;
    @Autowired
    private UserRepo userRepo;

    @Override
    public boolean add(Cart cart) {

        Product product = productRepo.findById(cart.getProduct().getProductId());
        if(product.getCount()>=cart.getCount()){
            log.info("Добавлен в корзину {}",cart);
            cartRepo.save(cart);
            return true;
        }
        return false;
    }

    @Override
    public List<Cart> getCartItemsByUserId(int userId) {
        User user = userRepo.findById(userId);
        List<Cart> cartList = cartRepo.findAllByUser(user);
        return cartList;
    }
}
